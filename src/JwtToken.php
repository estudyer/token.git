<?php
declare(strict_types=1);
namespace Markg\Token;

use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use stdClass;

class JwtToken
{
    public array $data = [];

    private array $config = [
        'secret' => 'A',
        'method' => 'HS256',
        'type' => 'Bearer',
        'upgrade' => 600
    ];

    public ?stdClass $decode;

    public bool $status = false;

    public string $message = '';

    /**
     * @param array $config
     */
    public function __construct(array $config = []) {
        $this->config = array_merge($this->config, $config);
    }

    /**
     * @return string
     */
    private function prefix(): string
    {
        return empty($this->config['type']) ? '' : ($this->config['type'] . ' ');
    }

    /**
     * @param array $data
     * @param int $ttl
     * @return string
     */
    public function create(array $data = [], int $ttl = 900): string
    {
        $payload = [
            'iss' => 'a',
            'aud' => 'b',
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + $ttl,
            'data' => $data
        ];

        return $this->prefix() . JWT::encode($payload, $this->config['secret'], $this->config['method'],'keyId');
    }

    /**
     * @param string $token
     * @return $this
     */
    public function verify(string $token): static
    {
        $prefix = $this->prefix();
        if (!empty($prefix) && !str_starts_with($token, $prefix)) {
            $token = '';
        }

        $token = str_replace($prefix, '', $token);

        try {
            $key = new Key($this->config['secret'], $this->config['method']);
            $this->decode = JWT::decode($token, $key);
            $this->data = json_decode(json_encode($this->decode->data), true);

            $this->status = true;
        } catch (\Throwable $exception) {
            $this->message = $exception->getMessage();
            $this->status = false;
        }

        return $this;
    }
}