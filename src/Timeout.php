<?php
declare(strict_types=1);
namespace Markg\Token;

class Timeout extends AuthAbstract
{
    protected array $config = [
        'type' => 'Timeout',
        'method' => 'HS256',
        'secret' => 'JwtTimeout',
        'upgrade' => false
    ];

    /**
     * @param array $data
     * @param int $ttl
     * @return string
     */
    public function generate(array $data, int $ttl = 900): string
    {
        return $this->generator->create($data, $ttl);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function verify(string $token = ''): bool
    {
        if (empty($token)) {
            $token = $_SERVER['HTTP_X_TIMEOUT'] ?? '';
        }

        return $this->generator->verify($token)->status;
    }
}