<?php
declare(strict_types=1);
namespace Markg\Token;

abstract class AuthAbstract
{
    protected array $config = [
        'type' => 'Bearer',
        'method' => 'HS256',
        'secret' => 'JwtToken',
        'upgrade' => false
    ];

    /**
     * @var JwtToken
     */
    protected JwtToken $generator;

    public function __construct(array $config = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->generator = new JwtToken($this->config);
    }

    /**
     * @return bool
     */
    public function status(): bool
    {
        return $this->generator->status;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->generator->message;
    }

    /**
     * @return int
     */
    public function expired(): int
    {
        if (!$this->status()) return 0;

        return (int) ($this->generator->decode->exp ?? 0);
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->generator->data;
    }

    /**
     * @param array $data
     * @param int $ttl
     * @return string
     */
    abstract public function generate(array $data, int $ttl = 900) :string;

    /**
     * @param string $token
     * @return bool
     */
    abstract public function verify(string $token = '') :bool;
}