<?php
declare(strict_types=1);
namespace Markg\Token;

class Login extends AuthAbstract
{
    /**
     * @param array $data
     * @param int $ttl
     * @return string
     */
    public function generate(array $data, int $ttl = 900): string
    {
        return $this->generator->create($data, $ttl);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function verify(string $token = ''): bool
    {
        if (empty($token)) {
            $token = $_SERVER['HTTP_X_TOKEN'] ?? '';
        }

        return $this->generator->verify($token)->status;
    }
}